<?php

/**
 * @file
 * Settings administration UI.
 */
//----------------------------------------------------------------------------
// Form API callbacks.

/**
 * Form definition; admin settings.
 */
function edgecast_cdn_admin_settings() {
  $form = array();

  $form['edgecast_cdn_customer'] = array(
    '#type' => 'textfield',
    '#title' => t('Edgecast Customer ID'),
    '#size' => 6,
    '#maxlength' => 6,
    '#default_value' => variable_get('edgecast_cdn_customer', ''),
    '#description' => t('The Edgecast customer id (6 characters).'),
  );

  $form['edgecast_cdn_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Edgecast API Token'),
    '#default_value' => variable_get('edgecast_cdn_api_token'),
    '#description' => t('The Edgecast token is used to make authenticated requests to clear the CDN cache. Find the token after logging in to my.edgecast.com and accessing "My Settings".'),
  );

  //$form['edgecast_cdn_path'] = array(
  //'#type' => 'textfield',
  //'#title' => t('Default Path'),
  //'#default_value' => variable_get('edgecast_cdn_path'),
  //'#description' => t('The default path used to clear the Edgecast CDN cache. No trailing slash.'),
  //);
  $form['edgecast_cdn_path'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#description' => t("Define all file paths being used for the website content.  This is the complete URL path in your control panel.  
      This includes CNAME paths if configured on EC.  Otherwise use the EC provided URL paths.  This must include the 
      protocol. One per line if you have multiple paths."),
    '#size' => 35,
    '#default_value' => variable_get('edgecast_cdn_path'),
  );
  //'#states' => array(
  //'visible' => array(
  //':input[name="' . CDN_MODE_VARIABLE . '"]' => array('value' => CDN_MODE_BASIC),
  //)
  //),
  //1: Windows Media 2: Flash 3: HTTP Large 8: HTTP Small
  $form['edgecast_cdn_service'] = array(
    '#type' => 'select',
    '#title' => t('Edgecast Service for Web Content'),
    '#options' => array(
      '3' => 'HTTP Large',
      '8' => 'HTTP Small'
    ),
    '#default_value' => variable_get('edgecast_cdn_service'),
    '#description' => t('Choose which service you are using for your web content.  This module currently only support using one service'),
  );

  return system_settings_form($form);
}

function edgecast_cdn_admin_token() {
  $form = array();

  $form['edgecast_cdn_token_pkey'] = array(
    '#type' => 'textfield',
    '#size' => 174,
    '#title' => t('Edgecast Private Key'),
    '#default_value' => variable_get('edgecast_cdn_token_pkey'),
    '#description' => t('Place your Primary private key for token authentication here.  This is available via your control panel.
      In each service on your EC panel there is a "Token Auth" menu item.  This is an optional service that must be activated
      for your account.  You must contact your sale agent to get this turned on. <b>This module only works with a single Private key.</b>
      if you are using multiple services for files you will need to make sure all the services have the same Private Key.'),
  );

  $form['edgecast_cdn_token_ecexpire'] = array(
    '#type' => 'textfield',
    '#size' => 15,
    '#title' => t('Edgecast Token Expire Time'),
    '#default_value' => variable_get('edgecast_cdn_token_ecexpire'),
    '#description' => t('Time in seconds that the link will expire. Leave blank if you do not want to use this setting. This
      is referred to as ec_expire in the EC documentation.'),
  );
  $form['edgecast_cdn_token_eccountryallow'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#title' => t('Edgecast Country Allow'),
    '#default_value' => variable_get('edgecast_cdn_token_eccountryallow'),
    '#description' => t('Countries allowed to get content.  This is a comma seperated list of ISO 3166 country codes.'),
  );
  $form['edgecast_cdn_token_eccountrydeny'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#title' => t('Edgecast Country Deny'),
    '#default_value' => variable_get('edgecast_cdn_token_eccountrydeny'),
    '#description' => t('Countries disallowed to get content.  This is a comma seperated list of ISO 3166 country codes.'),
  );
  $form['edgecast_cdn_token_ecrefallow'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#title' => t('Edgecast HTTP References Allowed'),
    '#default_value' => variable_get('edgecast_cdn_token_ecrefallow'),
    '#description' => t('HTTP Referals allowed to retrieve content.  It is possible to spoof this as a client is responsible for
      reporting it\'s own referal.  Also going from SSL to a non-SSL site it is disallowed in browsers for the referral to be sent.'),
  );
  $form['edgecast_cdn_token_ecrefdeny'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#title' => t('Edgecast HTTP References Denied'),
    '#default_value' => variable_get('edgecast_cdn_token_ecrefdeny'),
    '#description' => t('HTTP Referals denied to retrieve content.'),
  );
  $form['edgecast_cdn_token_ecprotoallow'] = array(
    '#type' => 'select',
    '#title' => t('Edgecast Allowed Protocols'),
    '#default_value' => variable_get('edgecast_cdn_token_ecprotoallow'),
    '#description' => t('Only two options: http or https. Defaults to both.'),
    '#options' => array(
      '0' => 'HTTP',
      '1' => 'HTTPS',
      '2' => 'BOTH'
    ),
  );
  $form['edgecast_cdn_token_ecprotodeny'] = array(
    '#type' => 'select',
    '#title' => t('Edgecast Deny Protocols'),
    '#default_value' => variable_get('edgecast_cdn_token_ecprotodeny'),
    '#description' => t('Only two options: http or https. Defaults to none. Denying both protocols is possible 
      but not for this module - doesn\'t make sense.'),
    '#options' => array(
      '0' => 'HTTP',
      '1' => 'HTTPS',
      '2' => 'NONE'
    ),
  );
  $form['edgecast_cdn_token_ecclientip'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#title' => t('Edgecast IP Addresses Allowed'),
    '#default_value' => variable_get('edgecast_cdn_token_ecclientip'),
    '#description' => t('IP Address to allow to connect to content.  Add IP\'s in IPv4 notation
      (e.g., 100.10.1.123), comma separate multiple values'),
  );
  $form['edgecast_cdn_token_ecurlallow'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#title' => t('Edgecast URL Allowed'),
    '#default_value' => variable_get('edgecast_cdn_token_ecurlallow'),
    '#description' => t('This is a not as used paramater where you have fine grained control of the URL\'s that can be accessed.
      This is actually the path after the domain name to the content.  I would suggest reading the EC Token Auth documentation for a
      very long explanation of thise settings.  It\'s too much to explain here. Comma seperate the values.'),
  );

  return system_settings_form($form);
}

function edgecast_cdn_selective_purge_form() {
  $form = array();

  $form['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths to Purge'),
    '#rows' => 6,
    '#description' => t('One path per line. Do not include the domain. For example, node/1234. To purge entire domain, enter *.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Purge CDN',
  );

  return $form;
}

function edgecast_cdn_selective_purge_form_submit($form_id, $form_state) {
  $temp = $form_state['values']['paths'];
  $paths = explode("\n", $temp);

  foreach ($paths AS $path) {
    $path = trim($path);
    edgecast_cdn_purge($path);
  }
}

function edgecast_cdn_testpage_form() {
  $vars = edgecast_cdn_settings_fields();
  $output ='';
  foreach ($vars as $items) {
    $output .= 'Variable: ' . $items . '&nbsp;&nbsp;Value: ' . variable_get($items);
    $output .= '<br /><br />';
  }
  $form = array();
  $form['#prefix'] = $output;
  return $form;

}